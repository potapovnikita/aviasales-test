import React, { useEffect, useState } from 'react';
import cn from './app.module.scss';
import api from './api';
import { APP } from './interfaces';
import { HooksTyping } from './utils/tshelpers'
import FilterContextProvider from './contexts/filterContext';

import Logo from './components/Logo';
import Tabs from './components/Tabs';
import Tickets from './components/Tickets';
import Filter from './components/Filter';

const App: React.FC = () => {
  const [searchId, setSearchId]: HooksTyping<string> = useState<string>('');
  const [allTickets, setAllTickets]: HooksTyping<APP.Ticket[] | []> = useState<APP.Ticket[] | []>([]);
  const [loader, setLoader]: HooksTyping<boolean> = useState<boolean>(false);

  useEffect(() => {
    getSearchId();
  }, []);

  useEffect(() => {
    if (searchId) getTickets();
  }, [searchId]);

  /** Get search id from server */
  const getSearchId = async () => {
    setLoader(true);
    try {
      const res = await api.getSearchId();
      const searchIdRes = res.data.searchId;
      if (searchIdRes) setSearchId(searchIdRes);
    } catch (e) {
      console.error(e);
    }
  };

  /** Get tickets pack from server */
  const getTickets = async () => {
    try {
      const { data: { tickets, stop }} = await api.getTickets(searchId);
      if (tickets.length) setAllTickets(prevAllTickets => [...tickets, ...prevAllTickets]);
      if (!stop) getTickets();
      else setLoader(false);
    } catch (e) {
      console.error(e);
      getTickets();
    }
  };

  return (
    <FilterContextProvider>
      <div className={cn.app}>
        <Logo/>
        <div className={cn.searchContainer}>
          <div className={cn.searchColumn}>
            <Filter loader={loader}/>
          </div>
          <div className={cn.searchColumn} onClick={() => {}}>
            <Tabs loader={loader}/>
            <Tickets tickets={allTickets} loader={loader}/>
          </div>
        </div>
      </div>
    </FilterContextProvider>
  );
};

export default App;
