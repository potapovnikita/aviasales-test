import React, { createContext, useState }from 'react';
import { APP } from '../interfaces';

interface IFilterContext {
  sort: string;
  filter: APP.ICheckboxState[];
  setFilter: (filter: APP.ICheckboxState[]) => void;
  setSort: (sort: string) => void;
  toggleSort: (type: APP.ICheckboxState[]) => void;
  toggleCheckbox: (toggledFilter: APP.ICheckboxState) => void;
}

export enum filterKeys {
  /** All tickets */
  ALL = 'All',
  /** No transfer */
  NO = 'No',
  /** 1 transfer */
  ONE = 'One',
  /** 2 transfers */
  TWO = 'Two',
  /** 3 transfers */
  THREE = 'Three',
}

export enum sortKeys {
  /** All tickets */
  LOWCOST = 'Lowcost',
  /** No transfer */
  FASTEST = 'Fastest',
}

const filterInitialState = [
  {
    name: 'Все',
    type: filterKeys.ALL,
    check: false,
    stops: 'All',
  },
  {
    name: 'Без пересадок',
    type: filterKeys.NO,
    check: false,
    stops: 0,
  },
  {
    name: '1 пересадка',
    type: filterKeys.ONE,
    check: true,
    stops: 1,
  },
  {
    name: '2 пересадки',
    type: filterKeys.TWO,
    check: true,
    stops: 2,
  },
  {
    name: '3 пересадки',
    type: filterKeys.THREE,
    check: false,
    stops: 3,
  }
];

export const initialState = {
  sort: sortKeys.LOWCOST,
  filter: filterInitialState,
};


/** hook for filter context*/
const useFilterHandler = (initialState: APP.IFilterState) => {
  const [sort, setSort] = useState(initialState.sort);
  const [filter, setFilter] = useState(initialState.filter);

  const toggleSort = (type: APP.ICheckboxState[]) => {
    setFilter(type);
  };

  const toggleCheckbox = (toggledFilter: APP.ICheckboxState) => {
    const newFilter = [...filter];
    newFilter.forEach(item => {
      if (toggledFilter.type === filterKeys.ALL) {
        item.check = item.type === filterKeys.ALL && !toggledFilter.check
          ? true
          : item.type !== filterKeys.ALL && toggledFilter.check;
      } else if (toggledFilter.type !== filterKeys.ALL && item.type === filterKeys.ALL) item.check = false;
      else if (toggledFilter.type === item.type) item.check = !toggledFilter.check;
    });

    if (newFilter.filter(checkbox => checkbox.check && checkbox.type !== filterKeys.ALL).length === filter.length - 1) {
      const allCheck = newFilter.find(checkbox => checkbox.type === filterKeys.ALL);
      if (allCheck) allCheck.check = true;
    }

    setFilter(newFilter);
  };

  return {
    sort,
    filter,
    setSort,
    setFilter,
    toggleSort,
    toggleCheckbox,
  };
};

/**
 * Create filter context
 */
export const filterContext = createContext<IFilterContext>({
  sort: initialState.sort,
  filter: initialState.filter,
  setSort: () => {},
  setFilter: () => {},
  toggleSort: () => {},
  toggleCheckbox: () => {},
});

const { Provider } = filterContext;

/**
 * Create provider filter context
 * @param children
 * @constructor
 */
const FilterContextProvider: React.FC<{ children: React.ReactNode }>  = ({ children }) => {
  const { sort, filter, setSort, setFilter, toggleSort, toggleCheckbox } = useFilterHandler(initialState);

  return (
    <Provider value={{ sort, filter, setSort, setFilter, toggleSort, toggleCheckbox }}>
      {children}
    </Provider>
  );
};

export default FilterContextProvider;
