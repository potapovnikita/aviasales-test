import React from 'react';
import cn from './checkbox.module.scss';

interface IProps {
  name: string;
  checked: boolean;
  toggle: () => void;
}


const Checkbox: React.FC<IProps> = ({ name, checked, toggle }: IProps) => {
  return (
    <div className={cn.checkboxContainer} onClick={() => { toggle(); }}>
      <span className={`${cn.checkbox} ${checked && cn.checked}`}/>
      <span className={cn.name}>{name}</span>
    </div>
  );
};

export default Checkbox;
