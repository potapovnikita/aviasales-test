import React  from 'react';
import cn from './logo.module.scss';
import logo from './logo.svg'

const Logo: React.FC = () => {
  return (
    <div className={cn.logo}>
      <img src={logo} alt="aviasales"/>
    </div>
  );
};

export default Logo;
