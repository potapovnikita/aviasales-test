import React, { useContext } from 'react';
import cn from './tabs.module.scss';

import { filterContext, sortKeys } from '../../contexts/filterContext';

interface IProps {
  loader: boolean;
}

interface ITab {
  name: string;
  type: string;
}

const tabs = [{
  name: 'Самый дешевый',
  type: sortKeys.LOWCOST,
}, {
  name: 'Самый быстрый',
  type: sortKeys.FASTEST,
}];

const Tabs: React.FC<IProps> = ({ loader }, IProps) => {
  const { sort, setSort } = useContext(filterContext);

  return (
    <div className={cn.tabsContainer}>
      <div className={`${loader && cn.disabled}`}/>
      {tabs.map((tab: ITab) =>
        <div className={`${cn.tab} ${tab.type === sort && cn.active}`}
             onClick={() => setSort(tab.type)}
             key={tab.type}>
        {tab.name}
      </div>)}
    </div>
  );
};

export default Tabs;
