import React from 'react';
import cn from './ticket.module.scss';

interface IProps {
  head: string;
  body: string;
}

const SegmentInfoItem:React.FC<IProps>  = ({ head, body }: IProps) => {
  return (
    <div className={cn.segmentItem}>
        <div className={cn.head}>{head}</div>
        <div className={cn.body}>{body}</div>
    </div>
  )
};

export default SegmentInfoItem;
