import React, { useContext } from 'react';
import cn from './filter.module.scss';

import { filterContext } from '../../contexts/filterContext';
import Checkbox from '../Checkbox'

interface IProps {
  loader: boolean;
}

const Filter: React.FC<IProps> = ({ loader }: IProps) => {
  const { filter, toggleCheckbox } = useContext(filterContext);


  return (
    <div className={cn.filterContainer}>
      <div className={`${loader && cn.disabled}`}/>
      <div className={cn.header}>Количество пересадок</div>
      {filter.map(checkbox => {
        return <Checkbox name={checkbox.name}
                         checked={checkbox.check}
                         toggle={() => {toggleCheckbox(checkbox);}}
                         key={checkbox.type}
        />
      })}
    </div>
  );
};

export default Filter;
