import React from 'react';
import cn from './ticket.module.scss';
import { CDN } from '../../api';
import { pluralizeAll, minutesToHours, getTimeInterval } from '../../utils/format';
import { APP } from '../../interfaces';

import SegmentInfoItem from './SegmentInfoItem';

interface IProps {
  ticket: APP.Ticket;
}

const STOP_DECLENSIONS = ['Пересадка', 'Пересадки', 'Пересадок'];

const Ticket: React.FC<IProps> = ({ ticket }: IProps) => {

  const getStopsText = (stops: number) => {
    return `${stops ? pluralizeAll(stops, STOP_DECLENSIONS) : 'Без пересадок'}`;
  };

  return (
    <div className={cn.ticket}>
      <div className={cn.header}>
        <div className={cn.price}>
          {ticket.price} P
        </div>
        <img className={cn.company} src={`//${CDN}/${ticket.carrier}.png`} alt={ticket.carrier}/>
      </div>

      {ticket.segments.length && ticket.segments.map((segment, index) => <div className={cn.segment} key={index}>
        <div className={cn.segmentRow}>
          <SegmentInfoItem head={`${segment.origin} - ${segment.destination}`}
                           body={`${getTimeInterval(segment.date, segment.duration)}`}
          />
          <SegmentInfoItem head="В пути"
                           body={`${minutesToHours(segment.duration)}`}
          />
          <SegmentInfoItem head={getStopsText(segment.stops.length)}
                           body={`${segment.stops.join(', ')}`}
          />
        </div>
      </div>)}
    </div>
  );
};


export default Ticket;
