import axios, { AxiosPromise } from 'axios';

export const CDN = 'pics.avs.io/99/36';
export const API_URL = 'https://front-test.beta.aviasales.ru';

export default {
  getSearchId(): AxiosPromise {
    return axios.get(`${API_URL}/search`)
  },

  getTickets(searchId: string): AxiosPromise {
    return axios.get(`${API_URL}/tickets?searchId=${searchId}`)
  },
};