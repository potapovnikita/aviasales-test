/**
 * Declension of numbers
 * @param {number} number
 * @param {string[]} titles
 * @return {string}
 */
export const pluralizeAll = (number: number, titles: string[]) => {
  const cases = [2, 0, 1, 1, 1, 2];

  const wordVariant: number = (number % 100 > 4 && number % 100 < 20)
    ? 2
    : cases[(number % 10 < 5)
      ? number % 10
      : 5];

  return `${number} ${titles[wordVariant]}`;
};

/**
 * Convert minutes to hours
 * @param {number} minutes
 * @return {string}
 */
export const minutesToHours = (minutes: number) => {
  return `${normalize(minutes / 60 | 0)}ч ${normalize(minutes % 60)}мин`;
};


/**
 * Get time interval
 * @param {string} startDate
 * @param {number} duration
 * @return {string}
 */
export const getTimeInterval = (startDate: string, duration: number) => {
  const date = new Date(startDate);
  const startTime = `${normalize(date.getHours())}:${normalize(date.getMinutes())}`;
  const finishDate = date.setMinutes(date.getMinutes() + duration);
  const finishTime = `${normalize(new Date(finishDate).getHours())}:${normalize(new Date(finishDate).getMinutes())}`;

  return `${startTime} - ${finishTime}`;
};

/**
 * Normalize date
 * @param time
 * @return {string}
 */
export const normalize = (time: any) => {
  return time < 10 ? `0${time}` : time
};