// generic для типизации хука
import {Dispatch, SetStateAction} from "react";

export type HooksTyping<hookType> = [hookType, Dispatch<SetStateAction<hookType>>];