import React  from 'react';
import cn from './empty.module.scss';

interface IProps {
  ticketsCount?: number;
}

const EmptyCard: React.FC<IProps> = ({ ticketsCount }: IProps) => {

  return (
    <div className={cn.emptyCard}>
      {ticketsCount ? <div className={cn.header}>
        Мы нашли {ticketsCount} рейсов, но ни один не соответствует заданным фильтрам.
      </div> : <div className={cn.header}>Ищем авиабилеты...</div>}
    </div>
  );
};

export default EmptyCard;
