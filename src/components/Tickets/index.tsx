import React, { useContext, useEffect, useState } from 'react';
import cn from './tickets.module.scss';
import { APP } from '../../interfaces';

import Ticket from '../Ticket';
import EmptyCard from '../EmptyCard';
import {filterContext, filterKeys, sortKeys} from "../../contexts/filterContext";
import { HooksTyping } from '../../utils/tshelpers';

interface IProps {
  tickets: APP.Ticket[];
  loader: boolean;
}

const Tickets: React.FC<IProps> = ({ tickets, loader }: IProps) => {
  const { sort, filter } = useContext(filterContext);
  const [filteredTickets, setFilteredTickets]: HooksTyping<APP.Ticket[]> = useState<APP.Ticket[]>([]);

  /** Sort tickets by price */
  const filterByPrice = (a: APP.Ticket, b: APP.Ticket) => a.price - b.price;

  /** Sort tickets by flight duration */
  const filterByDuration = (a: APP.Ticket, b: APP.Ticket) => {
    let sumDurationA = 0;
    let sumDurationB = 0;

    a.segments.map(({ duration }) => sumDurationA += duration);
    b.segments.map(({ duration }) => sumDurationB += duration);

    return sumDurationA - sumDurationB;
  };

  useEffect(() => {
    filterTickets();
  }, [sort, filter, tickets]);


  const filterTickets = () => {
    if (!tickets.length) return;
    let filtered = [...tickets];
    if (sort === sortKeys.FASTEST) filtered.sort(filterByDuration);
    else filtered.sort(filterByPrice);


    if (filter.length) {
      const stopsCount = filter
        .filter((item) => {
          if (item.check) return String(item.stops);
        })
        .map(stop => stop.stops);

      if (!stopsCount.find(stop => stop === filterKeys.ALL) || !stopsCount.length) {
        filtered = [...filtered].filter((ticket) => {
          const [firstSegment, secondSegment] = ticket.segments
          return stopsCount.includes(firstSegment.stops.length) && stopsCount.includes(secondSegment.stops.length)

        })
      }
    }
    setFilteredTickets(filtered.slice(0, 5));
  };

  return (
    <div className={cn.ticketsContainer}>
      {filteredTickets.length > 0 && filteredTickets.map((ticket, index) => <Ticket ticket={ticket} key={index}/>)}
      {(!filteredTickets.length || loader) && <EmptyCard ticketsCount={tickets.length}/>}
    </div>
  );
};

export default Tickets;
